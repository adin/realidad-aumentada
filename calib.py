"""
Ejemplo de como obtener la matriz de calibración de la camara.  
Es el código de los tutoriales de OpenCV.

Para obtener la matriz:
- Imprimir images/chessboard.png en una hoja.
- Ejecutar el código y poner la hoja al frente de la camara hasta que un patrón de lineas y puntos sea reconocido.
- Presionar 'q' para terminar la grabación de los puntos y que procese la calibración.

Note que entre más cuadros tenga mejor será la calibración pero el proceso demorará mucho más.
"""

import numpy as np
import cv2
import glob

# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((6*7,3), np.float32)
objp[:,:2] = np.mgrid[0:7,0:6].T.reshape(-1,2)

# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

images = glob.glob('*.jpg')

cv = cv2.VideoCapture(0)
ic, frame = cv.read()

# for fname in images:
while ic:
    # img = cv2.imread(fname)
    ic, img = cv.read()
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    # Find the chess board corners
    ret, corners = cv2.findChessboardCorners(gray, (7,6),None)

    # If found, add object points, image points (after refining them)
    if ret == True:
        objpoints.append(objp)

        corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
        imgpoints.append(corners2)

        # Draw and display the corners
        img = cv2.drawChessboardCorners(img, (7,6), corners2,ret)
    else:
        img = gray

    cv2.imshow('img',img)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)

print(mtx)

cv.release()
cv2.destroyAllWindows()
