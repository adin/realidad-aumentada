"""
Un ejemplo simple de realidad aumentada que utiliza un modelo (almacenado en una imagen) para buscarlo en un stream de imágenes de una camara.  Al encontrar la misma imágen del modelo, recupera la geometría de la imagen actual y despliega un modelo 3D en el stream.

Este script está inspirado por varios recursos y tutoriales en linea:
- https://rdmilligan.wordpress.com/2015/10/05/camera-pose-using-opencv-and-opengl/
- https://rdmilligan.wordpress.com/2015/10/15/augmented-reality-using-opencv-opengl-and-blender/
- https://bitesofcode.wordpress.com/2017/09/12/augmented-reality-with-python-and-opencv-part-1/
- https://bitesofcode.wordpress.com/2018/09/16/augmented-reality-with-python-and-opencv-part-2/

Modelos 3D de:
- https://clara.io/library

Representación de los objetos 3D:
- http://www.pygame.org/wiki/OBJFileLoader
"""

# OpenCV
import cv2

# Procesamiento de imágenes
import numpy as np
import math

# Linea de comando
import argparse



# Setup mágico
FRAME_WIDTH=640
FRAME_HEIGHT=480

# Número mínimo de emparejamientos
MIN_MATCHES = 10  
# Umbral de la distancia para considerar dos puntos iguales cuando calculamos la homografía
DIST_THR = 5.0

# Matriz de parámetros
# Pueden ajustar su cámara usando calib.py y ejecutando varias vezes
CAMERA_PARAM = np.array([[800, 0, FRAME_WIDTH/2], [0, 800, FRAME_HEIGHT/2], [0, 0, 1]])

# Color amarillo
YELLOW = (221, 170, 51)

def augment(modelPath = 'images/model.png', objectPath = 'models/fox.obj'):
  """
  Busca un modelo 2D y sobrepone el objeto 3D.
  """
  ## Inicializaciones
  homography = None 
  # Creamos el detector de características ORB
  orb = cv2.ORB_create()
  # Creamos el objeto que hace las comparaciones
  bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
  # Iniciamos la webcam
  vc = cv2.VideoCapture(0)
  # Establecemos el tamaño de las imágenes
  vc.set(cv2.CAP_PROP_FRAME_WIDTH, FRAME_WIDTH)
  vc.set(cv2.CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT)

  ## Procesamiento del modelo
  # Cargamos el modelo de referencia 2D
  model = cv2.imread(modelPath)
  # Compute model keypoints and its descriptors
  kpModel, desModel = orb.detectAndCompute(model, None)
  
  # Cargamos el modelo 3D
  obj = Object3D(objectPath, swapyz=True)  
  
  # Proceasmos el video por siempre hasta tener una tecla precionada
  while True:
    # read the current frame
    isCapturing, frame = vc.read()
    if not isCapturing:
      print("Error abriendo la camara")
      return 1

    # Encontramos los puntos de interes del cuadro actual
    kpFrame, desFrame = orb.detectAndCompute(frame, None)
    # Emparejamos los descriptores del modelo con el cuadro
    matches = bf.match(desModel, desFrame)
    # Los ordenamos de acuerdo a sus distancias (menor distancia entre los puntos es un mejor acierto)
    matches = sorted(matches, key=lambda x: x.distance)

    # Si tenemos suficientes aciertos calculamos las proyecciones
    if len(matches) > MIN_MATCHES:
      # Convertimos las dimensiones de los puntos en (# matches, 1, 2)
      modelPoints = np.float32([kpModel[m.queryIdx].pt for m in matches]).reshape(-1, 1, 2)
      framePoints = np.float32([kpFrame[m.trainIdx].pt for m in matches]).reshape(-1, 1, 2)
      # Calculamos la homografia (proyección)
      homography, _ = cv2.findHomography(modelPoints, framePoints, cv2.RANSAC, DIST_THR)
      # Si encontramos la proyección, entonces dibujamos 
      if homography is not None:
        if args.drawRectangle:
          # Dibujamos el rectangulo del modelo correspondiente
          h, w, _ = model.shape
          corners = np.float32([[0, 0], [0, h - 1], [w - 1, h - 1], [w - 1, 0]]).reshape(-1, 1, 2)
          # Proyectamos las esquinas en el cuadro actual
          newCorners = cv2.perspectiveTransform(corners, homography)
          # Conectamos las nuevas esquinas con lineas para dibujar el rectángulo
          frame = cv2.polylines(frame, [np.int32(newCorners)], True, 255, 3, cv2.LINE_AA)  

        try:
          # Obtenemos la proyección 3D de la homografia 2D y de los parámetros de la camara
          projection = projection3DMatrix(homography=homography)  
          # Proyectamos el objeto en el cuadro actual
          h, w, _ = model.shape
          frame = render(image=frame, object=obj, projection=projection, modelShape=(h, w))
        except Exception as e:
          print(f"Error tratando de projectar: {e}")
          pass
      # Dibujamos los aciertos
      if args.drawMatches:
        frame = cv2.drawMatches(model, kpModel, frame, kpFrame, matches[:10], 0, flags=2)
    # Sino tenemos los aciertos
    else:
      print(f"No hay suficientes aciertos: {len(matches)}/{MIN_MATCHES}")
    # Mostramos los resultados
    cv2.imshow("Presiona 'q' para terminar", frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
      break

  # Al terminar liberamos la camara
  vc.release()
  cv2.destroyAllWindows()
  # Retornamos que todo está OK
  return 0


# File loader http://www.pygame.org/wiki/OBJFileLoader
class Object3D:
  def __init__(self, filename, swapyz=False):
    """Loads a Wavefront OBJ file. """
    self.vertices = []
    self.normals = []
    self.texcoords = []
    self.faces = []
    material = None
    for line in open(filename, "r"):
      if line.startswith('#'): continue
      values = line.split()
      if not values: continue
      if values[0] == 'v':
        v = list(map(float, values[1:4]))
        if swapyz:
          v = v[0], v[2], v[1]
        self.vertices.append(v)
      elif values[0] == 'vn':
        v = list(map(float, values[1:4]))
        if swapyz:
          v = v[0], v[2], v[1]
        self.normals.append(v)
      elif values[0] == 'vt':
        self.texcoords.append(map(float, values[1:3]))
      elif values[0] == 'f':
        face = []
        texcoords = []
        norms = []
        for v in values[1:]:
          w = v.split('/')
          face.append(int(w[0]))
          if len(w) >= 2 and len(w[1]) > 0:
            texcoords.append(int(w[1]))
          else:
            texcoords.append(0)
          if len(w) >= 3 and len(w[2]) > 0:
            norms.append(int(w[2]))
          else:
            norms.append(0)
        self.faces.append((face, norms, texcoords))

def projection3DMatrix(homography = None, cameraParam = CAMERA_PARAM):
  """
  A partir de la matriz de calibración de la camara (cameraParam) y de la homografía estimada calculamos la matriz de proyección 3D
  """
  # Si tenemos una homografía mala, usamos el default de una matriz identidad
  if homography is None:
    homography = np.eye(3)

  # Calculamos la rotación en los eje x e y, y la traslación
  # Nuestra homografía está compuesta por las columnas [r1 r2 t], donde r1 y r2 son las columnas de la matriz de rotación 2D, y t es la traslación
  homography = homography * (-1)
  rotateAndTranslate = np.dot(np.linalg.inv(cameraParam), homography)
  r1 = rotateAndTranslate[:, 0]
  r2 = rotateAndTranslate[:, 1]
  t = rotateAndTranslate[:, 2]
  # Normalizamos los vectores
  l = math.sqrt(np.linalg.norm(r1, 2) * np.linalg.norm(r2, 2))
  rot1 = r1 / l
  rot2 = r2 / l
  translation = t / l

  ## Recuperamos la información 3D
  # Calculamos la base ortonormal
  c = rot1 + rot2
  p = np.cross(rot1, rot2)
  d = np.cross(c, p)
  rot1 = np.dot(c / np.linalg.norm(c, 2) + d / np.linalg.norm(d, 2), 1 / math.sqrt(2))
  rot2 = np.dot(c / np.linalg.norm(c, 2) - d / np.linalg.norm(d, 2), 1 / math.sqrt(2))
  rot3 = np.cross(rot1, rot2)
  # Calculamos la proyección del modelo actual
  projection = np.stack((rot1, rot2, rot3, translation)).T
  return np.dot(cameraParam, projection)
        
def render(image = None, object = None, projection = None, modelShape = None, color = YELLOW):
  """
  Sobrepone el objeto 3D en la imagen actual.  Depende del modelo para poder calcular donde centrar el modelo.

  Esta es una aproximación bien simplista de como reproducir los modelos 3D en las imagenes.  Existen librerias muy potentes para poder hacer este trabajo con muchas más opciones (por ejemplo, mostrar texturas o iluminación) como OpenGL o PyGame.  Esta es un área de estudio en si misma llamada Computer Graphics, y la idea del tutoria era dejar lo más simple posible.

  Recomiendo que revisen las biblioteas anteriores para mejorar el código.  O bien:
  https://rdmilligan.wordpress.com/2015/10/05/camera-pose-using-opencv-and-opengl/
  """
  vertices = object.vertices
  # Escalar los objectetos (caso a caso).  El '3' es un número mágico
  scaleMatrix = np.eye(3) * 3
  h, w = modelShape

  for face in object.faces:
    faceVertices = face[0]
    points = np.array([vertices[vertex - 1] for vertex in faceVertices])
    points = np.dot(points, scaleMatrix)
    # Reproducimos el objeto en la mitad del modelo detectado
    # Para eso tenemos que desplazar los puntos del modelo original
    points = np.array([[p[0] + w / 2, p[1] + h / 2, p[2]] for p in points])
    # Aplicamos la proyección para todos los puntos
    dst = cv2.perspectiveTransform(points.reshape(-1, 1, 3), projection)
    # Ajustamos el tipo de los puntos para que OpenCV esté feliz
    imagePts = np.int32(dst)
    # Usamos los puntos para dibujar un poligono
    # Los colores son BGR
    cv2.fillConvexPoly(image, imagePts, color[::-1])

  return image

def recordModel(modelPath = 'images/model.png'):
  # Variable de la camara web
  vc = cv2.VideoCapture(0)
  # Establecemos el tamaño de las imágenes
  vc.set(cv2.CAP_PROP_FRAME_WIDTH, FRAME_WIDTH)
  vc.set(cv2.CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT)

  isCapturing, frame = vc.read()
  while isCapturing:
    # Mostramos los resultados
    cv2.imshow("Presiona 'q' para guardar el cuadro actual", frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
      break
    isCapturing, frame = vc.read()
  # Escribimos la imagen
  cv2.imwrite(modelPath, frame)
  print(f"Modelo guardado en {modelPath}")
  return 0



# Argumentos de linea de comando
parser = argparse.ArgumentParser(description='Ejemplo de realidad aumentada')

parser.add_argument('-r','--rectangle', help = 'dibujar un rectangulo del modelo en el video', action = 'store_true', dest='drawRectangle')
parser.add_argument('-mk','--model-keypoints', help = 'dibujar los puntos de interés del modelo', action = 'store_true', dest='drawModelKp')
parser.add_argument('-fk','--frame-keypoints', help = 'dibujar los puntos de interés del video', action = 'store_true', dest='drawFrameKp')
parser.add_argument('-ma','--matches', help = 'dibujar los aciertos (emparejamientos) entre el modelo y el video', action = 'store_true', dest='drawMatches')

parser.add_argument('-m','--model', help = 'la ruta donde está el modelo que vamos a utilizar', dest='modelPath', default='images/model.png')
parser.add_argument('-o','--object', help = 'la ruta donde está el objeto 3D que vamos a utilizar', dest='objectPath', default='models/fox.obj')

parser.add_argument('-rm','--record-model', help = 'interfaz para grabar el model en lugar de procesarlo.  Usa la ruta del modelo (---model) para guardar la imagen', action = 'store_true', dest='recordModel')

args = parser.parse_args()

if __name__ == '__main__':
  if args.recordModel:
    recordModel(modelPath = args.modelPath)
  else:
    augment(modelPath = args.modelPath, objectPath = args.objectPath)
